#lang racket

; The compiler targets Python 3.  The main focus here is on generating
; a readable translation from the original FoxPro.  The goal is to
; remove the more tedious aspects of translation, not to produce a
; complete, runnable Python program that is equivalent to the original
; FoxPro.  At least for now, I expect that human review and revision
; of the generated code will be necessary.   
(require (prefix-in fp- "foxpro-ast.rkt"))
(require (prefix-in py- "python-ast.rkt"))
(provide (all-defined-out))

; COMPILATION
(define (map-type node)
  ; make a good guess as to what the type of a Python expr should be
  ; this could probably be a lot more sophisticated, but I am not going to
  ; expand it unless it's necessary
  (case (fp-expression-node-formal-type node)
    [(string) 'str]
    [(integer) 'int]
    [(float) 'float]
    [(boolean) 'bool]
    ; ...
    [else #f]))

(define (compile-node node)
  (cond
   [(fp-statement-node? node) (compile-statement-node node)]
   [(fp-expression-node? node) (compile-expression-node node)]
   [else (error "Unknown node type: " node)]))

(define (compile-statement-node node)
  (let ([ic (fp-statement-node-inline-comment node)]
        [compiled-node
         (cond
          [(fp-for-loop-node? node) (compile-for-loop-node node)]
          [(fp-assignment-node? node) (compile-assignment-node node)]
          ; ...
          [else (compile-node (fp-statement-node-child node))])])
    (py-set-statement-node-inline-comment! compiled-node ic)
    compiled-node))

(define (compile-assignment-node node)
  (py-make-assignment-node
   (compile-node (fp-assignment-node-identifier node))
   (compile-node (fp-assignment-node-expr node))))

(define (compile-for-loop-node node)
  (define (make-range-expr lower upper step)
    (py-simple-function-application
     (py-make-identifier-node "range")
     ; don't include the step if it's 1;
     ; don't include the lower bound if it's 0
     (if (and (py-literal-node? step) (string=? (py-expression-node-contents step) "1"))
       (if (and (py-literal-node? lower) (string=? (py-expression-node-contents lower) "0"))
           (list upper)
           (list lower upper))
       (list lower upper step))))
  
  (let ([target (compile-node (fp-for-loop-node-loop-var node))]
	[lower-bound (compile-node (fp-for-loop-node-init-val node))]
	[upper-bound
         ; VFP upper bound is inclusive; Python upper bound on range() is exclusive,
         ; so add 1
         ; TODO: 
         (py-make-binary-op-node
          "+"
          (compile-node (fp-for-loop-node-end-val node))
          (py-make-literal-node "1" #:type 'int))]
	[step (compile-node (fp-for-loop-node-step node))]
	[compiled-stmts (map compile-node (fp-for-loop-node-statements node))])
    (py-make-for-loop-node
     #:target target
     #:iterator (make-range-expr lower-bound upper-bound step)
     #:body compiled-stmts)))

(define (compile-expression-node node)
  (cond
   [(fp-literal-node? node) (compile-literal-node node)]
   [(fp-identifier-node? node) (compile-identifier-node node)]
   [(fp-unary-op-node? node) (compile-unary-op-node node)]
   [(fp-binary-op-node? node) (compile-binary-op-node node)]
   [(fp-member-reference-node? node) (compile-member-reference-node node)]
   [(fp-function-application-node? node) (compile-function-application-node node)]
   [else (error "Unknown expression type: " node)]))

(define (compile-literal-node node)
  (case (fp-expression-node-formal-type node)
   [(string) (compile-string-literal-node node)]
   [(boolean) (compile-boolean-literal-node node)]
   [(integer) (compile-integer-literal-node node)]
   [(float) (compile-float-literal-node node)]
   [else (error "Unknown literal type: " (fp-expression-node-formal-type node))]))

(define (compile-string-literal-node node)
  ; string literals come out of the lexer with delimiters intact
  ; here, we replace [] delimiters but leave others (",') alone
  ; we may also need to mess around with ampersands...not sure yet
  (let* ([s (fp-expression-node-contents node)]
	 [first-char (substring s 0 2)])
    (if (equal? first-char "[")
	(py-make-literal-node
	 (let* ([s1 (regexp-replace "\\[" s TRIPLE-QUOTE)]
		[s2 (regexp-replace "\\]" s1 TRIPLE-QUOTE)])
	   s2)
	 #:type 'str)
	(py-make-literal-node s #:type 'str))))

(define (compile-boolean-literal-node node)
  (let ([v (fp-expression-node-contents node)])
    (cond
     [(equal? v ".T.") (py-make-literal-node "True" #:type 'bool)]
     [(equal? v ".F.") (py-make-literal-node "False" #:type 'bool)]
     ; should this case map to None?
     [else (error "Unknown boolean literal: " node)])))

(define (compile-integer-literal-node node)
  ; passthrough for now...I'm not aware of any differences
  ; in VFP and Python integer literals, but there might be some
  (py-make-literal-node (fp-expression-node-contents node) #:type 'int))

(define (compile-float-literal-node node)
  ; again, passthrough for now
  (py-make-literal-node (fp-expression-node-contents node) #:type 'float))

(define (compile-identifier-node node)
  ; passthrough for now...I'm not aware of any differences in FoxPro vs.
  ; Python identifier syntax
  ; TODO: perhaps compile identifiers to be PEP8 compliant?
  ; - strip type indicator prefixes
  ; - convert camel case to underscores
  (py-make-identifier-node (fp-expression-node-contents node)
			   #:type (map-type node)))

(define (compile-member-reference-node node)
  ; again, passthrough for now
  (py-make-member-reference-node
   (compile-expression-node (fp-member-reference-node-object node))
   (compile-expression-node (fp-member-reference-node-member node))
   #:type (map-type node)))

(define (compile-unary-op-node node)
  (define (compile-operator op)
    (cond
      [(or (string=? op "!") (string=? op "NOT")) "not"]
      [else (error "Unknown unary operator: " op)]))
  (py-make-unary-op-node
   (compile-operator (fp-unary-op-node-operator node))
   (compile-expression-node (fp-unary-op-node-operand node))))

(define (compile-binary-op-node node)
  (define (compile-operator op)
    (cond
     [(string=? op "+") "+"] ; TODO: does FP + have same semantics?
     [(string=? op "-") "-"] ; TODO: does FP - have same semantics for non-string FP types?
     [(string=? op "**") "**"]
     [(string=? op "^") "**"]
     [(string=? op "*") "*"]
     [(string=? op "/") "/"] ; TODO: does FP division have same semantics?
     [(string=? op "=") "is"]
     [(string=? op "==") "=="] ; TODO: == is only defined for strings in VFP
     [(string=? op "<") "<"]
     [(string=? op "<=") "<="]
     [(string=? op ">") ">"]
     [(string=? op ">=") ">="]
     [(string=? op "!=") "!="]
     [(string=? op "<>") "!="]
     [(string=? op "#") "!="] ; TODO: VFP "#" can also be XOR?
     [(string=? op "AND") "and"]
     [(string=? op "OR") "or"]
     [else (error "Unknown binary operator: " op)]))

  (let* ([left (fp-binary-op-node-left-operand node)]
         [left-type (fp-expression-node-formal-type left)]
         [right (fp-binary-op-node-right-operand node)]
         [right-type (fp-expression-node-formal-type right)]
         [op (fp-binary-op-node-operator node)])
    (cond
      ; the VFP "-" operator on strings performs string concatenation after
      ; stripping whitespace from the left-hand string
      ; VFP: str1 - str2
      ; Python: str1.rstrip() + str2
      [(and (string=? op "-")
            (eq? left-type 'string)
            (eq? right-type 'string))
       (py-make-binary-op-node
        "+"    
        (py-simple-function-application
         (py-make-member-reference-node
          (compile-expression-node left)
          (py-make-identifier-node "rstrip"))
         '())
        (compile-expression-node right)
        #:type 'str)]
      ; the VFP "$" operator seeks the left operand in the right
      ; VFP: str1 $ str2
      ; Python: str2.find(str1)
      ; TODO: it's not clear from the MSDN documentation if this is actually the right
      ; behavior...
      [(string=? op "$")
       (py-simple-function-application
        (py-make-member-reference-node
         (compile-expression-node right)
         (py-make-identifier-node "find"))
        (compile-expression-node left))]
      [else
       (py-make-binary-op-node
        (compile-operator op)
        (compile-expression-node left)
        (compile-expression-node right))])))

(define (compile-function-application-node node)
  (let ([name (compile-node (fp-function-application-node-identifier node))]
        [args (map compile-node (fp-function-application-node-arg-list node))])
    (py-simple-function-application name args)))

; CODE GENERATION
(define INDENT "    ") 
(define NEWLINE "\n")
(define INLINE-COMMENT-CHAR "  # ")
(define TRIPLE-QUOTE "'''")
(define EXPR-SEP " ") ; separator between expressions, when called for
(define ARG-SEP ", ") ; separator between arg exprs in function calls

(define PYTHON3-OPERATOR-PRECS
  ; like the (precs...) declaration in the grammar, ordered highest to lowest
  ; quoted strings are literal operators; symbols name other expression types
  ; source: http://docs.python.org/py3k/reference/expressions.html#summary
  '((tuple-display list-display dict-display set-display)
    (subscript slice call attribute-reference)
    ("**")
    (bitwise-plus bitwise-minus bitwise-~)
    ("*" "/" "//" "%")
    ("+" "-")
    ("<<" ">>")
    ("&") ; bitwise AND
    ("^") ; bitwise XOR
    ("|") ; bitwise OR
    ("in" "not in" "is" "is not" "<" "<=" ">" ">=" "!=" "==")
    ("not") ; boolean NOT
    ("and") ; boolean AND
    ("or")  ; boolean OR
    (if-else)
    ("lambda")))
    
(define (generate-code node indent-level output-port)
  (cond
   [(py-statement-node? node) (gc-statement-node node indent-level output-port)]
   [(py-expression-node? node) (gc-expression-node node output-port)]
   [else (error "Cannot generate code for unknown node type: " node)]))

; code generation: statements
(define (display-multi #:to output-port . args)
  (for ([a args])
       (display a output-port)))

(define (gc-inline-comment node op)
  (let ([ic (py-statement-node-inline-comment node)])
      ; any inline comment?
      (when (not (string=? "" ic))
        (display-multi INLINE-COMMENT-CHAR ic #:to op))))

(define (gc-statement-node node indent-level op)
  ; take care of indentation here; statement block node code generators therefore
  ; do not need to indent their first lines
  (for ([i (in-range indent-level)])
       (display INDENT op))
  ; dispatch on node type
  (cond
   [(py-for-loop-node? node) (gc-for-loop-node node indent-level op)]
   [(py-assignment-node? node) (gc-assignment-node node indent-level op)]
   [(py-expression-node? (py-statement-node-child node))
    (begin (gc-expression-node (py-statement-node-child node) op)
           (gc-inline-comment node op))]
   [else (error "Cannot generate code for unknown statement type: " node)])
  ; end statement with a newline; statement node generators therefore do not
  ; need to add newlines (but may)
  (display NEWLINE op))

(define (gc-for-loop-node node indent-level op)
  (let ([te (py-for-loop-node-target-expr node)]
        [ie (py-for-loop-node-iterator-expr node)]
        [ic (py-statement-node-inline-comment node)]
        [bodys (py-for-loop-node-body-statements node)]
        [elses (py-for-loop-node-else-statements node)]
        [recur (lambda (s)
                 (gc-statement-node s (+ indent-level 1) op))])
    (display-multi "for" EXPR-SEP #:to op)
    (gc-expression-node te op)
    (display-multi EXPR-SEP "in" EXPR-SEP #:to op)
    (gc-expression-node ie op)
    (display ":" op)
    (gc-inline-comment node op)
    (display NEWLINE op)
    (map recur bodys)
    (when (not (null? elses))
      (display-multi "else:" NEWLINE #:to op)
      (map recur elses)
      (display NEWLINE op))))

(define (gc-assignment-node node indent-level op)
  (let ([name (py-assignment-node-identifier node)]
        [expr (py-assignment-node-expr node)])
    (gc-expression-node name op)
    (display-multi EXPR-SEP "=" EXPR-SEP #:to op)
    (gc-expression-node expr op)
    (gc-inline-comment node op)))

; code generation: expressions
(define (parens-required-for-child? node child)
  (define (operator-of n)
    (cond
     [(py-binary-op-node? n) (py-binary-op-node-operator n)]
     [(py-unary-op-node? n) (py-unary-op-node-operator n)]
     [(py-function-application-node? n) 'call]
     [(py-member-reference-node? n) 'attribute-reference]
     ; ...
     [else #f]))
  (define (level-in-precs op level precs)
    (if (or (null? precs)
            (member op (car precs)))
        level
        (level-in-precs op (+ 1 level) (cdr precs))))
  (define (higher-precedence? op1 op2)
    (cond
     [(eq? op1 #f) #t] ; no operator (indicated by #f) always wins
     [(eq? op2 #f) #f]
     ; a lower level in the precs hierarchy is higher precedence:
     [else (< (level-in-precs op1 0 PYTHON3-OPERATOR-PRECS)
              (level-in-precs op2 0 PYTHON3-OPERATOR-PRECS))]))

   ; parentheses are required if node has an operator of higher precedence than child
   ; TODO: what about left-right grouping?
  (higher-precedence? (operator-of node) (operator-of child)))

(define (gc-expression-node node op)
  (cond
   [(py-literal-node? node) (gc-literal-node node op)]
   [(py-identifier-node? node) (gc-identifier-node node op)]
   [(py-member-reference-node? node) (gc-member-reference-node node op)]
   [(py-function-application-node? node)
    (gc-function-application-node node op)]
   [(py-unary-op-node? node) (gc-unary-op-node node op)]
   [(py-binary-op-node? node) (gc-binary-op-node node op)]
   [else (error "Cannot generate code for unknown expression type:" node)]
   ))

(define (gc-literal-node node op)
  (display (py-expression-node-contents node) op))
(define (gc-identifier-node node op)
  (display (py-expression-node-contents node) op))
(define (gc-member-reference-node node op)
  (gc-expression-node (py-member-reference-node-object node) op)
  (display "." op)
  (gc-expression-node (py-member-reference-node-member node) op))

(define (gc-unary-op-node node op)
  (let* ([operator (py-unary-op-node-operator node)]
         [operand (py-unary-op-node-operand node)]
         [prq (parens-required-for-child? node operand)])
    (display-multi operator EXPR-SEP #:to op)
    (when prq (display "(" op))
    (gc-expression-node operand op)
    (when prq (display ")" op))))

(define (gc-binary-op-node node op)
  (let* ([operator (py-binary-op-node-operator node)]
         [left-operand (py-binary-op-node-left-operand node)]
         [right-operand (py-binary-op-node-right-operand node)]
         [prql (parens-required-for-child? node left-operand)]
         [prqr (parens-required-for-child? node right-operand)])
    (when prql (display "(" op))
    (gc-expression-node left-operand op)
    (when prql (display ")" op))
    (display-multi EXPR-SEP operator EXPR-SEP #:to op)
    (when prqr (display "(" op))
    (gc-expression-node right-operand op)
    (when prqr (display ")" op))))

(define (gc-function-application-node node op)
  (define (gc-arg-expr ae op)
    (cond
     [(py-expression-node? ae) ; positional argument
      (gc-expression-node ae op)]
     [(pair? ae) ; keyword argument
      (begin (gc-expression-node (car ae) op)
             (display "=" op)
             (gc-expression-node (cdr ae)))]
     [else (error "Unknown argument expression type: " ae)]))
  (define (gc-arg-list args op)
    (if (null? (cdr args))
        ; no more args after this one; just display the arg expr
        (gc-arg-expr (car args) op)
        ; otherwise, display the arg expr, a separator, and recurse
        (begin
          (gc-arg-expr (car args) op)
          (display ARG-SEP op)
          (gc-arg-list (cdr args) op))))

  (let* ([fname-node (py-function-application-node-name node)]
         [arg-list (py-function-application-node-arg-list node)]
         [kw-arg-list (py-function-application-node-kw-arg-list node)]
         ; not worrying about *args or **kwargs for now, since the
         ; compiler doesn't generate them
         [all-args-list (append arg-list kw-arg-list)])
    (gc-expression-node fname-node op)
    (display "(" op)
    (when (not (null? all-args-list)) (gc-arg-list all-args-list op))
    (display ")" op)))
