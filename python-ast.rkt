#lang racket
(provide (all-defined-out))

; AST nodes for Python language constructs
(struct node ()	#:transparent)

; MODULES
(struct module-node node
	(library-imports
	 local-imports
	 docstring
	 statements)
	#:transparent)

; STATEMENT NODES
(struct statement-node node
	(child inline-comment)
        #:mutable
	#:transparent)

(struct for-loop-node statement-node
	(target-expr iterator-expr body-statements else-statements)
	#:transparent)
(define (make-for-loop-node #:target t #:iterator i #:body b #:else [e '()]
                            #:inline-comment [ic #f])
  (for-loop-node #f ic t i b e))

(struct assignment-node statement-node
        (identifier expr)
        #:transparent)
(define (make-assignment-node id expr #:inline-comment [ic #f])
  (assignment-node #f ic id expr))

; EXPRESSION NODES
(define PYTHON3-TYPES
  '(bool
    (numeric (int float complex))
    (sequence (str bytes bytearray list tuple range))
    (set (set frozenset))
    dict
    ; probably won't need any of these:
    (iterator (iterator generator))
    memoryview
    context-manager
    code
    type
    null
    ellipsis))

(struct expression-node node
	(contents type) ;TODO
	#:transparent)

(struct literal-node expression-node
	()
	#:transparent)
(define (make-literal-node contents #:type [typ #f])
  (literal-node contents typ))

(struct identifier-node expression-node
	()
	#:transparent)
(define (make-identifier-node identifier #:type [typ #f])
  (identifier-node identifier typ))

(struct member-reference-node expression-node
	(object member)
	#:transparent)
(define (make-member-reference-node obj mem #:type [typ #f])
  (member-reference-node #f typ obj mem))

(struct unary-op-node expression-node
	(operator operand)
	#:transparent)
(define (make-unary-op-node operator operand #:type [typ #f])
  (unary-op-node #f typ operator operand))

(struct binary-op-node expression-node
	(operator left-operand right-operand)
	#:transparent)
(define (make-binary-op-node operator left-operand right-operand
			     #:type [typ #f])
  (binary-op-node #f typ operator left-operand right-operand))

(struct function-application-node expression-node
	(name arg-list kw-arg-list *args-expr **args-expr)
        #:transparent)
(define (simple-function-application name arg-list)
  ; shortcut constructor for function application nodes with
  ; no kwargs or varargs
  (function-application-node #f #f name arg-list '() #f #f))