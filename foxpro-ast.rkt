#lang racket
; AST nodes for FoxPro language constructs

; NOTE: a lot of this code (and the code in python-ast.rkt) is
; extremely repetitive right now, and begging to be simplified with
; the right set of macros.  I am holding off on doing so for now,
; since I expect that functions that are basically duplicates of one
; another now will become less redundant as the compiler evolves.

(provide (all-defined-out))

(struct node ()	#:transparent)

; there are three basic node types: statement, expression, comment

; STATEMENT NODES
; the parser wraps any statement in a statement-node so that the compiler
; can tell if an expression is also a statement, and can generate
; appropriate surrounding whitespace
; statement nodes track their own inline comments
(struct statement-node node
	(child inline-comment)
        #:mutable
	#:transparent)
(define (make-statement-node child #:inline-comment [ic #f])
  (statement-node child ic))

; simple statements formed only from keywords (e.g. "LOOP") get a
; keyword-statement-node containing the keywords as a list of symbols
(struct keyword-statement-node statement-node
	(symbols)
	#:transparent)
(define (make-keyword-statement-node syms #:inline-comment [ic #f])
  (keyword-statement-node #f ic syms))

(struct assignment-node statement-node
	(identifier expr)
	#:transparent)
(define (make-assignment-node id expr #:inline-comment [ic #f])
  (assignment-node #f ic id expr))

; compound statements each get their own node type (since each will need
; to be handled specially by the compiler)
; each still inherits from statement-node so that statement-node? is true
; of them; but the statement-node fields may not be used
(struct for-loop-node statement-node
	(loop-var init-val end-val step statements)
	#:transparent)
(define (make-for-loop-node #:loop-var lv
			    #:init [iv 0]
			    #:end ev
			    #:step [s 1] ; either (Racket) number or expr!
			    #:statements [sts '()]
			    #:inline-comment [ic #f])
  (for-loop-node #f ic lv iv ev s sts))

; EXPRESSION NODES
; expression nodes track type information when available:
; formal types are explicitly provided as FoxPro declarations
; informal types are string prefixes on identifiers
(struct expression-node node
	(contents
	 [formal-type #:mutable]
	 [informal-type #:mutable])
	#:transparent)
(define (make-expression-node contents
			      #:formal-type [ft #f]
			      #:informal-type [it #f])
  (expression-node contents ft it))

(struct literal-node expression-node
	()
	#:transparent)
(define (make-literal-node contents
			   #:formal-type [ft #f]
			   #:informal-type [it #f])
  (literal-node contents ft it))

(struct identifier-node expression-node
	()
	#:transparent)
(define (make-identifier-node identifier
			      #:formal-type [ft #f]
			      #:informal-type [it #f])
  (identifier-node identifier ft it))

(struct unary-op-node expression-node
	(operator operand)
	#:transparent)
(define (make-unary-op-node operator operand
			    #:formal-type [ft #f]
			    #:informal-type [it #f])
  (unary-op-node #f ft it operator operand))

(struct binary-op-node expression-node
	(operator left-operand right-operand)
	#:transparent)
(define (make-binary-op-node operator left-operand right-operand
			     #:formal-type [ft #f]
			     #:informal-type [it #f])
  (binary-op-node #f ft it operator left-operand right-operand))

(struct member-reference-node expression-node
	(object member)
	#:transparent)
(define (make-member-reference-node obj mem
				    #:formal-type [ft #f]
				    #:informal-type [it #f])
  (member-reference-node #f ft it obj mem))

(struct function-application-node expression-node
	(identifier arg-list)
	#:transparent)
(define (make-function-application-node identifier arg-list
					#:formal-type [ft #f]
					#:informal-type [it #f])
  (function-application-node #f ft it identifier arg-list))

(define FOXPRO-FORMAL-TYPES
  '((numeric (integer float))
    string
    boolean
    ; ...
    ))

(define FOXPRO-KNOWN-INFORMAL-TYPES
  (make-hash
   '(("c" . 'string)
     ;...
     )))

; construct as much type information as possible for an expression node
(define (infer-type expr-node)
  ; TODO
  ; unary operator nodes are always of logical type (since the only unary op
  ; is negation)
  ; binary op nodes with AND, OR, or a comparison are logical;
  #f)

; comment nodes store block (not inline) comments that should be
; included in the output
(struct comment-node node
	(contents)
	#:extra-constructor-name make-comment-node
	#:transparent)

